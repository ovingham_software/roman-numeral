document.addEventListener('submit', event => {
    event.preventDefault();
    translateDecimalToRoman()
});

function translateDecimalToRoman() {
    hideElements(['errorDiv', 'translationDiv']);
    lookupTranslation(getElementValue("decimalValue"), showTranslation, showError);
}

function lookupTranslation(decimalValue, successHandler, errorHandler) {
    const url = 'http://localhost:8080/api/translate/' + decimalValue;
    fetch(url)
        .then(
            (response) => {
                if (response.ok) {
                    response.json().then((data) => {
                        successHandler(data.romanNumeral);
                    });
                    return;
                }

                response.json().then((data) => {
                    errorHandler(data.message);
                });
            }
        )
        .catch((error) => {
            console.log('Fetch Error :-S', error);
            errorHandler('Oops something went wrong - please try again later.');
        });
}

function showTranslation(romanValue) {
    setElementInnerText("romanValue", (romanValue !== 0 ? romanValue : '&nbsp;'));
    showElement('translationDiv');
}

function showError(message) {
    setElementInnerText("errorMessage", message);
    showElement('errorDiv');
}

function hideElements(elementIds) {
    elementIds.forEach((elementId) => {
        hideElement(elementId)
    });
}

function hideElement(elementId) {
    document.getElementById(elementId).classList.add('d-none');
}

function showElement(elementId) {
    document.getElementById(elementId).classList.remove('d-none');
}

function getElementValue(elementId) {
    return document.getElementById(elementId).value;
}

function setElementInnerText(elementId, text) {
    document.getElementById(elementId).innerText = text;
}
