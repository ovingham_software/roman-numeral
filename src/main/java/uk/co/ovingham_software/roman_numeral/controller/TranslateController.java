package uk.co.ovingham_software.roman_numeral.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import uk.co.ovingham_software.roman_numeral.model.TranslationResponse;
import uk.co.ovingham_software.roman_numeral.service.RomanTranslationService;

@RestController
public class TranslateController {

    private final RomanTranslationService romanTranslationService;

    @Autowired
    public TranslateController(RomanTranslationService romanTranslationService) {
        this.romanTranslationService = romanTranslationService;
    }

    @GetMapping(path = "api/translate/{decimalValue}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public TranslationResponse translate(@PathVariable("decimalValue") Integer decimalValue) {
        return TranslationResponse.builder().romanNumeral(romanTranslationService.translate(decimalValue)).build();
    }
}
