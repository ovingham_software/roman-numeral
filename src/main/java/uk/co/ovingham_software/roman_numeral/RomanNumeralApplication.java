package uk.co.ovingham_software.roman_numeral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomanNumeralApplication {

    public static void main(String[] args) {
        SpringApplication.run(RomanNumeralApplication.class, args);
    }
}
