package uk.co.ovingham_software.roman_numeral.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TranslationResponse {
    private String romanNumeral;
}
