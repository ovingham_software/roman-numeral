package uk.co.ovingham_software.roman_numeral.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class TranslationMapConfiguration {

    @Bean
    public Map<Integer, String> unitsTranslation() {
        return Map.of(0, "",
                1, "I",
                2, "II",
                3, "III",
                4, "IV",
                5, "V",
                6, "VI",
                7, "VII",
                8, "VIII",
                9, "IX");
    }

    @Bean
    public Map<Integer, String> tensTranslation() {
        return Map.of(0, "",
                1, "X",
                2, "XX",
                3, "XXX",
                4, "XL",
                5, "L",
                6, "LX",
                7, "LXX",
                8, "LXXX",
                9, "XC");
    }

    @Bean
    public Map<Integer, String> hundredsTranslation() {
        return Map.of(0, "",
                1, "C",
                2, "CC",
                3, "CCC",
                4, "CD",
                5, "D",
                6, "DC",
                7, "DCC",
                8, "DCCC",
                9, "CM");
    }

    @Bean
    public Map<Integer, String> thousandsTranslation() {
        return Map.of(0, "",
                1, "M",
                2, "MM",
                3, "MMM");
    }
}
