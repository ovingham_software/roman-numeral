package uk.co.ovingham_software.roman_numeral.service;

import org.springframework.stereotype.Service;

@Service
public class PlaceValueServiceImpl implements PlaceValueService {

    @Override
    public Integer unitsValue(Integer value) {
        return value % 10;
    }

    @Override
    public Integer tensValue(Integer value) {
        return (value / 10) % 10;
    }

    @Override
    public Integer hundredsValue(Integer value) {
        return (value / 100) % 10;
    }

    @Override
    public Integer thousandsValue(Integer value) {
        return (value / 1000);
    }
}
