package uk.co.ovingham_software.roman_numeral.service.translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UnitsTranslator implements RomanNumeralTranslator {
    private final Map<Integer, String> translation;

    @Autowired
    public UnitsTranslator(Map<Integer, String> unitsTranslation) {
        this.translation = unitsTranslation;
    }

    @Override
    public String translate(Integer unitsValue) {
        return translation.getOrDefault(unitsValue, "");
    }
}
