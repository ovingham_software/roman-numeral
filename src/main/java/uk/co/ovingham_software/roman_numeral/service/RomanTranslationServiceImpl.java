package uk.co.ovingham_software.roman_numeral.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.co.ovingham_software.roman_numeral.service.translator.RomanNumeralTranslator;

@Service
public class RomanTranslationServiceImpl implements RomanTranslationService {

    private final TranslationValidator translationValidator;
    private final PlaceValueService placeValueService;
    private final RomanNumeralTranslator unitsTranslator;
    private final RomanNumeralTranslator tensTranslator;
    private final RomanNumeralTranslator hundredsTranslator;
    private final RomanNumeralTranslator thousandsTranslator;

    @Autowired
    public RomanTranslationServiceImpl(TranslationValidator translationValidator,
                                       PlaceValueService placeValueService,
                                       RomanNumeralTranslator unitsTranslator,
                                       RomanNumeralTranslator tensTranslator,
                                       RomanNumeralTranslator hundredsTranslator,
                                       RomanNumeralTranslator thousandsTranslator) {
        this.translationValidator = translationValidator;
        this.placeValueService = placeValueService;
        this.unitsTranslator = unitsTranslator;
        this.tensTranslator = tensTranslator;
        this.hundredsTranslator = hundredsTranslator;
        this.thousandsTranslator = thousandsTranslator;
    }

    @Override
    public String translate(Integer value) {
        translationValidator.validate(value);
        return thousandsTranslator.translate(placeValueService.thousandsValue(value)) +
                hundredsTranslator.translate(placeValueService.hundredsValue(value)) +
                tensTranslator.translate(placeValueService.tensValue(value)) +
                unitsTranslator.translate(placeValueService.unitsValue(value));
    }
}
