package uk.co.ovingham_software.roman_numeral.service;

import org.springframework.stereotype.Service;
import uk.co.ovingham_software.roman_numeral.exception.ValidationErrorException;

@Service
public class TranslationValidatorImpl implements TranslationValidator {

    @Override
    public void validate(Integer value) {
        if ((isNotGreaterThanOrEqualToZero(value) || isNotLessThanOrEqualToThreeThousand(value))) {
            throw new ValidationErrorException("Error: decimal value to translate must be greater than or equal to 0 and less than or equal to 3,000.");
        }
    }

    private boolean isNotGreaterThanOrEqualToZero(Integer value) {
        return !isGreaterThanOrEqualToZero(value);
    }

    private boolean isNotLessThanOrEqualToThreeThousand(Integer value) {
        return !isLessThanOrEqualToThreeThousand(value);
    }

    private boolean isGreaterThanOrEqualToZero(Integer value) {
        return value.compareTo(0) >= 0;
    }

    private boolean isLessThanOrEqualToThreeThousand(Integer value) {
        return value.compareTo(3000) <= 0;
    }
}
