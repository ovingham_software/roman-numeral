package uk.co.ovingham_software.roman_numeral.service.translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ThousandsTranslator implements RomanNumeralTranslator {
    private final Map<Integer, String> translation;

    @Autowired
    public ThousandsTranslator(Map<Integer, String> thousandsTranslation) {
        this.translation = thousandsTranslation;
    }

    @Override
    public String translate(Integer thousandsValue) {
        return translation.getOrDefault(thousandsValue, "");
    }
}
