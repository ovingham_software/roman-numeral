package uk.co.ovingham_software.roman_numeral.service;

public interface TranslationValidator {
    void validate(Integer value);
}
