package uk.co.ovingham_software.roman_numeral.service.translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class TensTranslator implements RomanNumeralTranslator {
    private final Map<Integer, String> translation;

    @Autowired
    public TensTranslator(Map<Integer, String> tensTranslation) {
        this.translation = tensTranslation;
    }

    @Override
    public String translate(Integer tensValue) {
        return translation.getOrDefault(tensValue, "");
    }
}
