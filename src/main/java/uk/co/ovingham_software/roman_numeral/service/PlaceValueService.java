package uk.co.ovingham_software.roman_numeral.service;

public interface PlaceValueService {
    Integer unitsValue(Integer value);

    Integer tensValue(Integer value);

    Integer hundredsValue(Integer value);

    Integer thousandsValue(Integer value);
}
