package uk.co.ovingham_software.roman_numeral.service.translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class HundredsTranslator implements RomanNumeralTranslator {
    private final Map<Integer, String> translation;

    @Autowired
    public HundredsTranslator(Map<Integer, String> hundredsTranslation) {
        this.translation = hundredsTranslation;
    }

    @Override
    public String translate(Integer hundredsValue) {
        return translation.getOrDefault(hundredsValue, "");
    }
}
