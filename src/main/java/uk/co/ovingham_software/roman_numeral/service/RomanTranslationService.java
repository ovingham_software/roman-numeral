package uk.co.ovingham_software.roman_numeral.service;

public interface RomanTranslationService {
    String translate(Integer value);
}
