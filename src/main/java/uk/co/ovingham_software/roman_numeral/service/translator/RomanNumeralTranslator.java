package uk.co.ovingham_software.roman_numeral.service.translator;

public interface RomanNumeralTranslator {
    String translate(Integer value);
}
