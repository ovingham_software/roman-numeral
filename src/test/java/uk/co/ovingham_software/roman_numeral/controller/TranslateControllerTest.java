package uk.co.ovingham_software.roman_numeral.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.co.ovingham_software.roman_numeral.service.RomanTranslationService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TranslateControllerTest {

    @InjectMocks
    private TranslateController translateController;

    @Mock
    RomanTranslationService romanTranslationService;

    @Test
    public void shouldTranslate() {
        when(romanTranslationService.translate(eq(64))).thenReturn("LXIV");
        assertThat(translateController.translate(64).getRomanNumeral()).isEqualTo("LXIV");
        verify(romanTranslationService).translate(eq(64));
    }
}