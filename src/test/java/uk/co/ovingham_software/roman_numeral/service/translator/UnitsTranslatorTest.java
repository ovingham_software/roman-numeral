package uk.co.ovingham_software.roman_numeral.service.translator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import uk.co.ovingham_software.roman_numeral.configuration.TranslationMapConfiguration;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class UnitsTranslatorTest {

    private RomanNumeralTranslator unitsTranslator;

    @BeforeEach
    public void setUp() {
        var translationMapConfiguration = new TranslationMapConfiguration();
        unitsTranslator = new UnitsTranslator(translationMapConfiguration.unitsTranslation());
    }

    @ParameterizedTest(name = "Translate Units [{0}] to [{1}]")
    @MethodSource("unitsInputAndExpectedRomanNumeralProvider")
    public void shouldTranslateUnitsToRoman(Integer unitsInput, String expectedRomanNumeral) {
        assertThat(unitsTranslator.translate(unitsInput)).isEqualTo(expectedRomanNumeral);
    }

    private static Stream<Arguments> unitsInputAndExpectedRomanNumeralProvider() {
        return Stream.of(
                Arguments.of(0, ""),
                Arguments.of(1, "I"),
                Arguments.of(2, "II"),
                Arguments.of(3, "III"),
                Arguments.of(4, "IV"),
                Arguments.of(5, "V"),
                Arguments.of(6, "VI"),
                Arguments.of(7, "VII"),
                Arguments.of(8, "VIII"),
                Arguments.of(9, "IX")
        );
    }
}