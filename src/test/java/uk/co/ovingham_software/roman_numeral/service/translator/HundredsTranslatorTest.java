package uk.co.ovingham_software.roman_numeral.service.translator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import uk.co.ovingham_software.roman_numeral.configuration.TranslationMapConfiguration;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class HundredsTranslatorTest {
    private RomanNumeralTranslator hundredsTranslator;

    @BeforeEach
    public void setUp() {
        var translationMapConfiguration = new TranslationMapConfiguration();
        hundredsTranslator = new HundredsTranslator(translationMapConfiguration.hundredsTranslation());
    }

    @ParameterizedTest(name = "Translate Hundreds [{0}] to [{1}]")
    @MethodSource("hundredsInputAndExpectedRomanNumeralProvider")
    public void shouldTranslateHundredsToRoman(Integer hundredsInput, String expectedRomanNumeral) {
        assertThat(hundredsTranslator.translate(hundredsInput)).isEqualTo(expectedRomanNumeral);
    }

    private static Stream<Arguments> hundredsInputAndExpectedRomanNumeralProvider() {
        return Stream.of(
                Arguments.of(0, ""),
                Arguments.of(1, "C"),
                Arguments.of(2, "CC"),
                Arguments.of(3, "CCC"),
                Arguments.of(4, "CD"),
                Arguments.of(5, "D"),
                Arguments.of(6, "DC"),
                Arguments.of(7, "DCC"),
                Arguments.of(8, "DCCC"),
                Arguments.of(9, "CM")
        );
    }
}