package uk.co.ovingham_software.roman_numeral.service.translator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import uk.co.ovingham_software.roman_numeral.configuration.TranslationMapConfiguration;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class TensTranslatorTest {

    private RomanNumeralTranslator tensTranslator;

    @BeforeEach
    public void setUp() {
        var translationMapConfiguration = new TranslationMapConfiguration();
        tensTranslator = new TensTranslator(translationMapConfiguration.tensTranslation());
    }

    @ParameterizedTest(name = "Translate Tens [{0}] to [{1}]")
    @MethodSource("tensInputAndExpectedRomanNumeralProvider")
    public void shouldTranslateTensToRoman(Integer tensInput, String expectedRomanNumeral) {
        assertThat(tensTranslator.translate(tensInput)).isEqualTo(expectedRomanNumeral);
    }

    private static Stream<Arguments> tensInputAndExpectedRomanNumeralProvider() {
        return Stream.of(
                Arguments.of(0, ""),
                Arguments.of(1, "X"),
                Arguments.of(2, "XX"),
                Arguments.of(3, "XXX"),
                Arguments.of(4, "XL"),
                Arguments.of(5, "L"),
                Arguments.of(6, "LX"),
                Arguments.of(7, "LXX"),
                Arguments.of(8, "LXXX"),
                Arguments.of(9, "XC")
        );
    }
}