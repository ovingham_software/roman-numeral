package uk.co.ovingham_software.roman_numeral.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class PlaceValueServiceImplTest {

    private PlaceValueService placeValueService;

    @BeforeEach
    public void setUp() {
        placeValueService = new PlaceValueServiceImpl();
    }

    @ParameterizedTest(name = "Units Value from [{0}]")
    @MethodSource("unitsValueProvider")
    public void shouldGetUnitsValue(Integer inputValue, Integer expectedUnitsValue) {
        assertThat(placeValueService.unitsValue(inputValue)).isEqualTo(expectedUnitsValue);
    }

    private static Stream<Arguments> unitsValueProvider() {
        return Stream.of(
                Arguments.of(1, 1),
                Arguments.of(23, 3),
                Arguments.of(456, 6),
                Arguments.of(7891, 1)
        );
    }

    @ParameterizedTest(name = "Tens Value from [{0}]")
    @MethodSource("tensValueProvider")
    public void shouldGetTensValue(Integer inputValue, Integer expectedTensValue) {
        assertThat(placeValueService.tensValue(inputValue)).isEqualTo(expectedTensValue);
    }

    private static Stream<Arguments> tensValueProvider() {
        return Stream.of(
                Arguments.of(1, 0),
                Arguments.of(23, 2),
                Arguments.of(456, 5),
                Arguments.of(7891, 9)
        );
    }

    @ParameterizedTest(name = "Hundreds Value from [{0}]")
    @MethodSource("hundredsValueProvider")
    public void shouldGetHundredsValue(Integer inputValue, Integer expectedHundredsValue) {
        assertThat(placeValueService.hundredsValue(inputValue)).isEqualTo(expectedHundredsValue);
    }

    private static Stream<Arguments> hundredsValueProvider() {
        return Stream.of(
                Arguments.of(1, 0),
                Arguments.of(23, 0),
                Arguments.of(456, 4),
                Arguments.of(7891, 8)
        );
    }

    @ParameterizedTest(name = "Thousands Value from [{0}]")
    @MethodSource("thousandsValueProvider")
    public void shouldGetThousandsValue(Integer inputValue, Integer expectedThousandsValue) {
        assertThat(placeValueService.thousandsValue(inputValue)).isEqualTo(expectedThousandsValue);
    }

    private static Stream<Arguments> thousandsValueProvider() {
        return Stream.of(
                Arguments.of(1, 0),
                Arguments.of(23, 0),
                Arguments.of(456, 0),
                Arguments.of(7891, 7)
        );
    }
}