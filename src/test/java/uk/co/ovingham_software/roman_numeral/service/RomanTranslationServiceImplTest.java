package uk.co.ovingham_software.roman_numeral.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import uk.co.ovingham_software.roman_numeral.configuration.TranslationMapConfiguration;
import uk.co.ovingham_software.roman_numeral.service.translator.HundredsTranslator;
import uk.co.ovingham_software.roman_numeral.service.translator.TensTranslator;
import uk.co.ovingham_software.roman_numeral.service.translator.ThousandsTranslator;
import uk.co.ovingham_software.roman_numeral.service.translator.UnitsTranslator;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class RomanTranslationServiceImplTest {

    private RomanTranslationService romanTranslationService;

    @BeforeEach
    public void setUp() {
        var translationValidator = new TranslationValidatorImpl();
        var placeValueService = new PlaceValueServiceImpl();
        var translationMapConfiguration = new TranslationMapConfiguration();
        var unitsTranslator = new UnitsTranslator(translationMapConfiguration.unitsTranslation());
        var tensTranslator = new TensTranslator(translationMapConfiguration.tensTranslation());
        var hundredsTranslator = new HundredsTranslator(translationMapConfiguration.hundredsTranslation());
        var thousandsTranslator = new ThousandsTranslator(translationMapConfiguration.thousandsTranslation());
        romanTranslationService = new RomanTranslationServiceImpl(
                translationValidator, placeValueService, unitsTranslator, tensTranslator, hundredsTranslator, thousandsTranslator);
    }

    @ParameterizedTest(name = "Translate Decimal [{0}] to [{1}]")
    @MethodSource("decimalInputAndExpectedRomanNumeralProvider")
    public void shouldTranslateDecimalToRoman(Integer decimalInput, String expectedRomanNumeral) {
        assertThat(romanTranslationService.translate(decimalInput)).isEqualTo(expectedRomanNumeral);
    }

    static Stream<Arguments> decimalInputAndExpectedRomanNumeralProvider() {
        return Stream.of(
                Arguments.of(1, "I"),
                Arguments.of(2, "II"),
                Arguments.of(3, "III"),
                Arguments.of(4, "IV"),
                Arguments.of(5, "V"),
                Arguments.of(6, "VI"),
                Arguments.of(7, "VII"),
                Arguments.of(8, "VIII"),
                Arguments.of(9, "IX"),
                Arguments.of(10, "X"),
                Arguments.of(11, "XI"),
                Arguments.of(12, "XII"),
                Arguments.of(13, "XIII"),
                Arguments.of(14, "XIV"),
                Arguments.of(15, "XV"),
                Arguments.of(16, "XVI"),
                Arguments.of(17, "XVII"),
                Arguments.of(18, "XVIII"),
                Arguments.of(19, "XIX"),
                Arguments.of(20, "XX"),
                Arguments.of(30, "XXX"),
                Arguments.of(40, "XL"),
                Arguments.of(50, "L"),
                Arguments.of(60, "LX"),
                Arguments.of(70, "LXX"),
                Arguments.of(80, "LXXX"),
                Arguments.of(90, "XC"),
                Arguments.of(100, "C"),
                Arguments.of(101, "CI"),
                Arguments.of(111, "CXI"),
                Arguments.of(200, "CC"),
                Arguments.of(300, "CCC"),
                Arguments.of(400, "CD"),
                Arguments.of(500, "D"),
                Arguments.of(600, "DC"),
                Arguments.of(700, "DCC"),
                Arguments.of(800, "DCCC"),
                Arguments.of(900, "CM"),
                Arguments.of(1000, "M"),
                Arguments.of(1001, "MI"),
                Arguments.of(1021, "MXXI"),
                Arguments.of(1321, "MCCCXXI"),
                Arguments.of(2000, "MM"),
                Arguments.of(3000, "MMM")
        );
    }
}