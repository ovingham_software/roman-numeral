package uk.co.ovingham_software.roman_numeral.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import uk.co.ovingham_software.roman_numeral.exception.ValidationErrorException;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class TranslationValidatorImplTest {

    private TranslationValidator translationValidator;

    @BeforeEach
    public void setUp() {
        translationValidator = new TranslationValidatorImpl();
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 1000, 2999, 3000})
    public void shouldPassValidValues(Integer value) {
        assertThatCode(() -> {
            translationValidator.validate(value);
        }).doesNotThrowAnyException();
    }

    @ParameterizedTest
    @ValueSource(ints = {-32, -1, 3001, 4000})
    public void shouldFailInValidValues(Integer value) {
        assertThatThrownBy(() -> translationValidator.validate(value))
                .isInstanceOf(ValidationErrorException.class)
                .hasMessage("Error: decimal value to translate must be greater than or equal to 0 and less than or equal to 3,000.");
    }
}