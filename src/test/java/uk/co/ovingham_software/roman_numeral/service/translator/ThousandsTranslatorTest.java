package uk.co.ovingham_software.roman_numeral.service.translator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import uk.co.ovingham_software.roman_numeral.configuration.TranslationMapConfiguration;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ThousandsTranslatorTest {
    private RomanNumeralTranslator thousandsTranslator;

    @BeforeEach
    public void setUp() {
        var translationMapConfiguration = new TranslationMapConfiguration();
        thousandsTranslator = new ThousandsTranslator(translationMapConfiguration.thousandsTranslation());
    }

    @ParameterizedTest(name = "Translate Thousands [{0}] to [{1}]")
    @MethodSource("thousandsInputAndExpectedRomanNumeralProvider")
    public void shouldTranslateThousandsToRoman(Integer thousandsInput, String expectedRomanNumeral) {
        assertThat(thousandsTranslator.translate(thousandsInput)).isEqualTo(expectedRomanNumeral);
    }

    private static Stream<Arguments> thousandsInputAndExpectedRomanNumeralProvider() {
        return Stream.of(
                Arguments.of(0, ""),
                Arguments.of(1, "M"),
                Arguments.of(2, "MM"),
                Arguments.of(3, "MMM")
        );
    }
}