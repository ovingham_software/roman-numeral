### Roman Numeral Translator Kata

Convert decimal integers into Roman numerals coding exercise.

#### Author

John Turner

Freelance Java Engineer, Architect and Leader

https://www.linkedin.com/in/johnedwardturner/

https://ovingham-software.co.uk/

#### Build &amp; Run

./gradlew bootRun

#### API

**Input:** Decimal integer between 0 and 3,000 (inclusive) 

**Request:** HTTP GET http://localhost:8080/api/translate/ &lt;decimal value&gt;

**Response:** {"romanNumeral": "&lt;Roman value&gt;"}

**Examples:**

| Request | Response |
| --- | --- |
| curl http://localhost:8080/api/translate/64 | `{"romanNumeral":"LXIV"}` |
| curl http://localhost:8080/api/translate/1024 | `{"romanNumeral":"MXXIV"}` |

#### UI

**URL:** http://localhost:8080/index.html

#### Roman Numeral Background Information

https://en.wikipedia.org/wiki/Roman_numerals
